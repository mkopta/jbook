#!/usr/bin/env python3
import json


def print_index(index):
    for page, picture in sorted(index.items()):
        if picture is None:
            print(page)
        else:
            print(f'{page}\t{picture}')


def load_index(filename='index.json'):
    with open(filename, 'r') as f:
        index = json.load(f)
    return {int(page): picture for page, picture in index.items()}


def save_index(index):
    with open('index.json', 'w') as f:
        json.dump(index, f, indent=2)
        f.write('\n')


def print_page(page, picture):
    if picture is not None:
        print(f'{page}\t{picture}')
    else:
        print(page)


def print_index(index):
    for page, picture in sorted(index.items()):
        print_page(page, picture)


def print_stats(index):
    total = len(index)
    done = len([p for p in index.values() if p is not None])
    percent_done = int(done / (total / 100.0))
    remaining = total - done
    print(f'Total pictures: {total}')
    print(f'Done:           {done} ({percent_done}%)')
    print(f'Remaining:      {remaining}')


def edit(index):
    page = input('page? ')
    try:
        page = int(page)
    except:
        print('Not a number.')
        return
    if page not in index:
        print('Out of range.')
        return
    print_page(page, index[page])
    picture = input('picture? ')
    index[page] = picture
    print_page(page, index[page])


def print_help():
    print('i  print index')
    print('s  print stats')
    print('e  edit')
    print('l  load index from disk')
    print('w  write index to disk')
    print('q  quit')


index = load_index()
while True:
    cmd = input('> ').lower()
    if cmd == 'q':
        break
    elif cmd == 'l':
        index = load_index()
    elif cmd == 'w':
        save_index(index)
    elif cmd == 'i':
        print_index(index)
    elif cmd == 's':
        print_stats(index)
    elif cmd == 'e':
        edit(index)
    else:
        print_help()
